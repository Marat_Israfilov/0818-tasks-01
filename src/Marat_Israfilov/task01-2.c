/*Программа преобразует величину роста из американской системы (футы'дюймы) в
европейскую (см).*/
#include <stdio.h>

int main()
{
	float inch, foot, cm;
	char answer = ' ';
	printf("Enter your height in feet and inches:\n");
	scanf("%f'%f", &foot, &inch);

	while((foot < 0) || (inch < 0))
	{
		scanf("%c", &answer);
		
		if(answer == 'n')
		{
			return 0;
		}
		else if(answer == 'y')
		{
			printf("Enter your height in feet and inches:\n");
			scanf("%f %f", &foot, &inch);
		}
		else
		{
			printf("Wrong number! Do you want to enter the number again?(y/n)\n");
		}			
	}

	inch += foot * 12;
	cm = inch * 2.54;
	printf("Your height: %.2f cm.\n", cm);
	return 0;
}
